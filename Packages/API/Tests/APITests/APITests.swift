import XCTest
@testable import API

final class APITests: XCTestCase {
    func testBadURLResponseFailure() {
        let exp = expectation(description: "Should throw APIError.badResponse")
        let session = URLSessionMock(
            data: nil,
            response: HTTPURLResponse.badResponse,
            error: nil
        )
        let api = API<RequestMock>(
            session: session,
            environment: .init(
                host: URL(string: "https://www.google.nl")!,
                apiKey: ""
            ),
            urlFormatter: { _, url in url }
        )
        let request = RequestMock(endPoint: URL(string: "https://www.google.nl")!)
        api.fetch(request) { (result: Result<NoResponse, Error>) in
            do {
                _ = try result.get()
                XCTFail("There shouldn't be any data here")
            } catch APIError.badResponse {
                exp.fulfill()
            } catch {
                return
            }
        }

        waitForExpectations(timeout: 1, handler: nil)
    }

    func testNoURLResponseFailure() {
        let exp = expectation(description: "Should throw APIError.noHTTPURLResponse")
        let session = URLSessionMock(
            data: nil,
            response: nil,
            error: nil
        )
        
        let api = API<RequestMock>(
            session: session,
            environment: .init(
                host: URL(string: "https://www.google.nl")!,
                apiKey: ""
            ),
            urlFormatter: { _, url in url }
        )
        let request = RequestMock(endPoint: URL(string: "https://www.google.nl")!)
        api.fetch(request) { (result: Result<NoResponse, Error>) in
            do {
                _ = try result.get()
                XCTFail("There shouldn't be any data here")
            } catch APIError.noHTTPURLResponse {
                exp.fulfill()
            } catch {
                return
            }
        }

        waitForExpectations(timeout: 1, handler: nil)
    }

    func testNoDataError() {
        let exp = expectation(description: "Should throw APIError.noData")
        let session = URLSessionMock(
            data: nil,
            response: HTTPURLResponse.goodResponse,
            error: nil
        )
        
        let api = API<RequestMock>(
            session: session,
            environment: .init(
                host: URL(string: "https://www.google.nl")!,
                apiKey: ""
            ),
            urlFormatter: { _, url in url }
        )
        let request = RequestMock(endPoint: URL(string: "https://www.google.nl")!)
        api.fetch(request) { (result: Result<NoResponse, Error>) in
            do {
                _ = try result.get()
                XCTFail("There shouldn't be any data here")
            } catch APIError.noData {
                exp.fulfill()
            } catch {
                return
            }
        }

        waitForExpectations(timeout: 1, handler: nil)
    }

    func testSimpleResponseSuccess() {
        let exp = expectation(description: "Should successfully decode")

        let expectedReponse = SimpleResponse(string: "test", Int: 0)
        let data = try! JSONEncoder().encode(expectedReponse)

        let session = URLSessionMock(
            data: data,
            response: HTTPURLResponse.goodResponse,
            error: nil
        )
        
        let api = API<RequestMock>(
            session: session,
            environment: .init(
                host: URL(string: "https://www.google.nl")!,
                apiKey: ""
            ),
            urlFormatter: { _, url in url }
        )
        let request = RequestMock(endPoint: URL(string: "https://www.google.nl")!)
        api.fetch(request) { (result: Result<SimpleResponse, Error>) in
            do {
                let response = try result.get()
                if response == expectedReponse {
                    exp.fulfill()
                }
            } catch {
                XCTFail("Decoding failed")
            }
        }

        waitForExpectations(timeout: 1, handler: nil)
    }

    func testResponseCorruptDataFailure() {
        let exp = expectation(description: "Should throw dataCorrupted error")

        let data = Data()

        let session = URLSessionMock(
            data: data,
            response: HTTPURLResponse.goodResponse,
            error: nil
        )
        
        let api = API<RequestMock>(
            session: session,
            environment: .init(
                host: URL(string: "https://www.google.nl")!,
                apiKey: ""
            ),
            urlFormatter: { _, url in url }
        )
        let request = RequestMock(endPoint: URL(string: "https://www.google.nl")!)
        api.fetch(request) { (result: Result<SimpleResponse, Error>) in
            do {
                _ = try result.get()
            } catch DecodingError.dataCorrupted {
                exp.fulfill()
            } catch {
                return
            }
        }

        waitForExpectations(timeout: 1, handler: nil)
    }

    func testResponseMissingKeysFailure() {
        let exp = expectation(description: "Should fail to decode")

        let badResponse = NoResponse()

        let data = try! JSONEncoder().encode(badResponse)

        let session = URLSessionMock(
            data: data,
            response: HTTPURLResponse.goodResponse,
            error: nil
        )
        
        let api = API<RequestMock>(
            session: session,
            environment: .init(
                host: URL(string: "https://www.google.nl")!,
                apiKey: ""
            ),
            urlFormatter: { _, url in url }
        )
        let request = RequestMock(endPoint: URL(string: "https://www.google.nl")!)
        api.fetch(request) { (result: Result<SimpleResponse, Error>) in
            do {
                _ = try result.get()
            } catch DecodingError.keyNotFound {
                exp.fulfill()
            } catch {
                return
            }
        }

        waitForExpectations(timeout: 1, handler: nil)
    }

    func testError() {
        let exp = expectation(description: "Should throw error")

        let expectedError = NSError(domain: "apitests", code: 1, userInfo: nil)

        let session = URLSessionMock(
            data: nil,
            response: nil,
            error: expectedError
        )
        
        let api = API<RequestMock>(
            session: session,
            environment: .init(
                host: URL(string: "https://www.google.nl")!,
                apiKey: ""
            ),
            urlFormatter: { _, url in url }
        )
        let request = RequestMock(endPoint: URL(string: "https://www.google.nl")!)
        api.fetch(request) { (result: Result<SimpleResponse, Error>) in
            do {
                _ = try result.get()
            } catch {
                if expectedError == error as NSError {
                    exp.fulfill()
                }
            }
        }

        waitForExpectations(timeout: 1, handler: nil)
    }
}

struct NoResponse: Codable { }

struct SimpleResponse: Codable, Equatable {
    let string: String
    let Int: Int
}
