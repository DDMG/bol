import Foundation
import UIKit

public enum APIError: Error {
    case noHTTPURLResponse
    case noData
    case badResponse
}

public protocol APIProtocol {
    associatedtype EP: EndPoint
    
    @discardableResult
    func fetch<Model: Decodable>(
        _ request: EP,
        completion: @escaping (Result<Model, Error>) -> ()
    ) -> URLSessionDataTask
}

public protocol EndPoint {
    var endPoint: URL { get }
}

public class API<EP: EndPoint>: APIProtocol {
    // MARK: Debugging
    private var debug: Bool = false
    private var errorLogger: (Error) -> () = { _ in }
    
    private let session: URLSession
    private let environment: Environment
    private let urlFormatter: (_ environment: Environment, _ url: URL) -> URL?
    
    public init(
        session: URLSession = .shared,
        environment: Environment,
        urlFormatter: @escaping (_ environment: Environment, _ url: URL) -> URL?
    ) {
        self.session = session
        self.environment = environment
        self.urlFormatter = urlFormatter
    }

    /**
     Fetchs data from some endpoint and maps it to a model
     */
    @discardableResult
    public func fetch<Model: Decodable>(
        _ request: EP,
        completion: @escaping (Result<Model, Error>) -> ()
    ) -> URLSessionDataTask {
        let path = urlFormatter(environment, request.endPoint).map({ URLRequest(url: $0) })!
        let completion: (Result<Model, Error>) -> () = { [debug, errorLogger] result in
            if case .failure(let error) = result, debug { errorLogger(error) }
            completion(result)
        }
        let dataTask = session.dataTask(with: path) { data, response, error in
            DispatchQueue.main.async {

                if let error = error {
                    completion(.failure(error))
                    return
                }

                guard let httpResponse = response as? HTTPURLResponse else {
                    completion(.failure(APIError.noHTTPURLResponse))
                    return
                }

                guard httpResponse.isSuccessful else {
                    completion(.failure(APIError.badResponse))
                    return
                }

                if let data = data {
                    do {
                        let model = try JSONDecoder().decode(Model.self, from: data)
                        completion(.success(model))
                    } catch {
                        completion(.failure(error))
                    }
                } else {
                    completion(.failure(APIError.noData))
                }
            }
        }

        dataTask.resume()

        return dataTask
    }
    
    public func debug(
        _ flag: Bool,
        errorLogger: @escaping (Error) -> ()
    ) -> Self {
        self.debug = flag
        self.errorLogger = errorLogger
        return self
    }
}

fileprivate extension HTTPURLResponse {
    var isSuccessful: Bool {
        return (200..<300).contains(statusCode)
    }
}
