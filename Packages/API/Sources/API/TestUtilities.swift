//
//  File.swift
//  
//
//  Created by Derek Schade on 24/05/2020.
//

import Foundation

public struct RequestMock: EndPoint {
    public let endPoint: URL
}

public class URLSessionMock: URLSession {
    let data: Data?
    let response: URLResponse?
    let error: Error?

    public init(data: Data?, response: URLResponse?, error: Error?) {
        self.data = data
        self.response = response
        self.error = error
    }

    public override func dataTask(
        with request: URLRequest,
        completionHandler: @escaping (Data?, URLResponse?, Error?) -> Void
    ) -> URLSessionDataTask {
        URLSessionDataTaskMock {
            completionHandler(self.data, self.response, self.error)
        }
    }
}

public class URLSessionDataTaskMock: URLSessionDataTask {
    private let closure: () -> Void

    public init(closure: @escaping () -> Void) {
        self.closure = closure
    }

    public override func resume() {
        closure()
    }
}

public extension HTTPURLResponse {
    static var goodResponse: HTTPURLResponse {
        HTTPURLResponse(url: URL(string: "https://www.google.nl")!, statusCode: 200, httpVersion: nil, headerFields: nil)!
    }

    static var badResponse: HTTPURLResponse {
        HTTPURLResponse(url: URL(string: "https://www.google.nl")!, statusCode: 0, httpVersion: nil, headerFields: nil)!
    }
}
