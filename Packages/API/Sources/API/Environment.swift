//
//  File.swift
//  
//
//  Created by Derek Schade on 09/12/2020.
//

import Foundation

public struct Environment {
    public let host: URL
    public let apiKey: String

    public init(
        host: URL,
        apiKey: String
    ) {
        self.host = host
        self.apiKey = apiKey
    }
}
