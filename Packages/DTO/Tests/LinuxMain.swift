import XCTest

import DTOTests

var tests = [XCTestCaseEntry]()
tests += DTOTests.allTests()
XCTMain(tests)
