import Foundation

// MARK: - Products
public struct Products: Codable {
    public init(products: [Product]) {
        self.products = products
    }
    
    public let products: [Product]
}

// MARK: - Product
public struct Product: Codable {
    public init(id: String, ean: String, gpc: String, title: String, specsTag: String, rating: Int, shortDescription: String, longDescription: String, attributeGroups: [AttributeGroup], entityGroups: [EntityGroup], urls: [URLElement], images: [Image], media: [Image], offerData: OfferData, parentCategoryPaths: [ParentCategoryPath]) {
        self.id = id
        self.ean = ean
        self.gpc = gpc
        self.title = title
        self.specsTag = specsTag
        self.rating = rating
        self.shortDescription = shortDescription
        self.longDescription = longDescription
        self.attributeGroups = attributeGroups
        self.entityGroups = entityGroups
        self.urls = urls
        self.images = images
        self.media = media
        self.offerData = offerData
        self.parentCategoryPaths = parentCategoryPaths
    }
    
    public let id, ean, gpc, title: String
    public let specsTag: String
    public let rating: Int
    public let shortDescription, longDescription: String
    public let attributeGroups: [AttributeGroup]
    public let entityGroups: [EntityGroup]
    public let urls: [URLElement]
    public let images, media: [Image]
    public let offerData: OfferData
    public let parentCategoryPaths: [ParentCategoryPath]
}

// MARK: - AttributeGroup
public struct AttributeGroup: Codable {
    public let title: String
    public let attributes: [Attribute]
}

// MARK: - Attribute
public struct Attribute: Codable {
    public let label, value: String
    public let key: String?
}

// MARK: - EntityGroup
public struct EntityGroup: Codable {
    public let title: String
    public let entities: [Entity]?
}

// MARK: - Entity
public struct Entity: Codable {
    public let id, value: String
}

// MARK: - Image
public struct Image: Codable {
    public let type: TypeEnum
    public let key: String
    public let url: String
}

public enum TypeEnum: String, Codable {
    case image = "IMAGE"
}

// MARK: - OfferData
public struct OfferData: Codable {
    public init(bolCOM: Int?, nonProfessionalSellers: Int, professionalSellers: Int, offers: [Offer]) {
        self.bolCOM = bolCOM
        self.nonProfessionalSellers = nonProfessionalSellers
        self.professionalSellers = professionalSellers
        self.offers = offers
    }
    
    public let bolCOM: Int?
    public let nonProfessionalSellers, professionalSellers: Int
    public let offers: [Offer]
    
    enum CodingKeys: String, CodingKey {
        case bolCOM
        case nonProfessionalSellers, professionalSellers, offers
    }
}

// MARK: - Offer
public struct Offer: Codable {
    public init(id: String, condition: String, price: Double, availabilityCode: String, availabilityDescription: String, seller: Seller, bestOffer: Bool) {
        self.id = id
        self.condition = condition
        self.price = price
        self.availabilityCode = availabilityCode
        self.availabilityDescription = availabilityDescription
        self.seller = seller
        self.bestOffer = bestOffer
    }
    
    public let id, condition: String
    public let price: Double
    public let availabilityCode, availabilityDescription: String
    public let seller: Seller
    public let bestOffer: Bool
}

// MARK: - Seller
public struct Seller: Codable {
    public let id, sellerType, displayName: String
    public let topSeller: Bool
    public let logo: String
    public let sellerRating: SellerRating
    public let recentReviewCounts: RecentReviewCounts?
    public let sellerInformation: String?
    public let useWarrantyRepairConditions: Bool
    public let approvalPercentage, registrationDate: String
}

// MARK: - RecentReviewCounts
public struct RecentReviewCounts: Codable {
    public let positiveReviewCount, neutralReviewCount, negativeReviewCount, totalReviewCount: Int
}

// MARK: - SellerRating
public struct SellerRating: Codable {
    public let ratingMethod, sellerRating, productInformationRating, deliveryTimeRating: String
    public let shippingRating, serviceRating: String
}

// MARK: - ParentCategoryPath
public struct ParentCategoryPath: Codable {
    public let parentCategories: [ParentCategory]
}

// MARK: - ParentCategory
public struct ParentCategory: Codable {
    public let id, name: String
}

// Mpublic ARK: - URLElement
public struct URLElement: Codable {
    public let key: String
    public let value: String
}
