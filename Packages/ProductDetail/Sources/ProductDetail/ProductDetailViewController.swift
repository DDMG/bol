//
//  ProductDetailViewController.swift
//  Bol
//
//  Created by Derek Schade on 08/12/2020.
//

import Foundation
import UIKit
import SnapKit

final class ProductDetailViewController: UIViewController {
    private let imageView: UIImageView = {
        let view = UIImageView()
        view.contentMode = .scaleAspectFit
        return view
    }()
    private let activityIndicatorView: UIActivityIndicatorView = {
        let view = UIActivityIndicatorView(frame: CGRect(x: 0, y: 0, width: 50, height: 50))
        view.hidesWhenStopped = true
        view.style = .medium
        return view
    }()
    private let stackView: UIStackView = {
        let view = UIStackView()
        view.axis = .vertical
        view.spacing = 8
        return view
    }()
    private let scrollView: UIScrollView = {
        let view = UIScrollView()
        view.alwaysBounceVertical = true
        return view
    }()
    private let viewModel: ProductDetailViewModelInterface
    private let priceLabel: UILabel
    private let producerLabel: UILabel
    private let titleLabel: UILabel
    
    private let imageLoader: (_ url: String, _ imageView: UIImageView) -> ()
    private let alertPresenter: (_ error: Error, _ presenter: UIViewController) -> ()

    init(
        viewModel: ProductDetailViewModelInterface,
        priceLabel: UILabel = BolLabelFactory.label(.price),
        producerLabel: UILabel = BolLabelFactory.label(.subtitle),
        titleLabel: UILabel = BolLabelFactory.label(.title),
        imageLoader: @escaping (_ url: String, _ imageView: UIImageView) -> () = { url, imageView in ImageLoader.load(url: url, imageView: imageView) },
        alertPresenter: @escaping (_ error: Error, _ presenter: UIViewController) -> () = AlertPresenter.present(error:viewController:)
    ) {
        self.viewModel = viewModel
        self.priceLabel = priceLabel
        self.producerLabel = producerLabel
        self.titleLabel = titleLabel
        self.imageLoader = imageLoader
        self.alertPresenter = alertPresenter
        
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    public override func viewDidLoad() {
        super.viewDidLoad()
        defer { viewModel.viewDidLoad() }
        setupView()
    }
    
    private func setupView() {
        view.backgroundColor = .systemBackground
        view.addSubview(scrollView)
        view.addSubview(activityIndicatorView)
        
        stackView.addArrangedSubview(imageView)
        stackView.addArrangedSubview(priceLabel)
        stackView.addArrangedSubview(producerLabel)
        stackView.addArrangedSubview(titleLabel)
        
        scrollView.addSubview(stackView)
        scrollView.snp.makeConstraints { make in
            make.edges.equalToSuperview()
        }
        stackView.snp.makeConstraints { make in
            make.width.equalTo(view).inset(16)
            make.top.leading.trailing.equalToSuperview().inset(16)
        }
        activityIndicatorView.snp.makeConstraints { make in
            make.centerX.centerY.equalToSuperview()
        }
        imageView.snp.makeConstraints { make in
            make.height.equalTo(300)
        }
    }
    
}

extension ProductDetailViewController: ProductDetailViewModelDelegate {
    public func setLoading(
        flag: Bool
    ) {
        flag
            ? activityIndicatorView.startAnimating()
            : activityIndicatorView.stopAnimating()
    }
    
    public func set(
        result: Result<ProductDetailModel, Error>?
    ) {
        guard let result = result else { return }
        do {
            let model = try result.get()
            priceLabel.text = model.price
            producerLabel.text = model.producer
            titleLabel.text = model.title
            imageLoader(model.imageUrls[0], imageView)
        } catch {
            alertPresenter(error, self)
        }
    }
}

// MARK: - Previews

#if DEBUG
import SwiftUI
import DTO

extension ProductDetailEnvironment {
    
    /// Instantly returns a successfull response, decoded from products.json
    static var success: Self {
        .init { completion in
            completion(
                .success(
                    Products.load(fileName: "products")!
                )
            )
        }
    }
    
    /// Instantly returns an error
    public static var failure: Self {
        struct TestError: Error {
            let localizedDescription: String
        }
        return .init { completion in
            completion(.failure(TestError(localizedDescription: "hey"))
            )
        }
    }
    
    /// Never completing API call
    static var loading: Self {
        .init { _ in }
    }
}

struct ProductDetailViewController_Previews: PreviewProvider {
    
    static var previews: some View {
        ProductDetail.build(environment: .success).preview
        // Not sure why this is broken
        ProductDetail.build(environment: .failure).preview
        ProductDetail.build(environment: .loading).preview
    }
}
#endif
