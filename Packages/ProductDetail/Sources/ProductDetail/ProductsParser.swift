//
//  ProductsParser.swift
//  Bol
//
//  Created by Derek Schade on 08/12/2020.
//

import Foundation
import DTO

struct ProductsParser {
    
    init() { }
    
    func parse(
        products: Products,
        currencyFormatter: (Double) -> String = { value in
            let formatter = NumberFormatter()
            formatter.numberStyle = .currency
            formatter.locale = .current
            return formatter.string(from: value as NSNumber)!
        }
    ) -> ProductDetailModel {
        guard let product = products.products.first else { fatalError() }
        let price = product.offerData.offers.first?.price
        return .init(
            title: product.title,
            price: price.flatMap(currencyFormatter).flatMap({ "Price: \($0)" }) ?? "nan",
            producer: product.attributeGroups
                .first(where: {  $0.title == "Productinformatie" })?
                .attributes
                .first(where: { $0.label == "Merk" })?
                .value
                ?? "",
            rating: 123,
            imageUrls: product.media.map(\.url)
        )
    }
}
