//
//  ProductDetailViewModel.swift
//  Bol
//
//  Created by Derek Schade on 08/12/2020.
//

import Foundation
import DTO

protocol ProductDetailViewModelInterface: ViewDelegate { }

protocol ProductDetailViewModelDelegate: class {
    func setLoading(flag: Bool)
    func set(result: Result<ProductDetailModel, Error>?)
}

final class ProductDetailViewModel {
    private let environment: ProductDetailEnvironment
    public weak var delegate: ProductDetailViewModelDelegate?
    private let parser: (Products) -> (ProductDetailModel)
    
    private(set) var isLoading: Bool = false {
        didSet { delegate?.setLoading(flag: isLoading) }
    }
    private(set) var result: Result<ProductDetailModel, Error>? = nil {
        didSet { delegate?.set(result: result) }
    }
    
    public init(
        environment: ProductDetailEnvironment,
        parser: @escaping (Products) -> (ProductDetailModel) = { ProductsParser().parse(products: $0) }
    ) {
        self.environment = environment
        self.parser = parser
    }
    
    private func getProducts() {
        isLoading = true
        environment.getProducts { [weak self] result in
            guard let self = self else { return }
            self.isLoading = false
            self.result = result.map(self.parser)
        }
    }
}

// MARK: - ProductDetailViewModelInterface

extension ProductDetailViewModel: ProductDetailViewModelInterface {
    func viewDidLoad() {
        getProducts()
    }
}
