//
//  File.swift
//  
//
//  Created by Derek Schade on 09/12/2020.
//

import Foundation
import UIKit

public enum ProductDetail {
    /// Public build method
    public static func build(
        environment: ProductDetailEnvironment
    ) -> UIViewController {
        build(
            viewModel: ProductDetailViewModel(environment: environment),
            viewBuilder: { ProductDetailViewController(viewModel: $0) }
        )
    }
    
    /// Internal build method to wire up the delegate and help with testing
    static func build(
        viewModel: ProductDetailViewModel,
        viewBuilder: (ProductDetailViewModel) -> ProductDetailViewController = { ProductDetailViewController(viewModel: $0) }
    ) -> ProductDetailViewController {
        let view = viewBuilder(viewModel)
        viewModel.delegate = view
        return view
    }
}
