//
//  UIViewController+Preview.swift
//  Bol
//
//  Created by Derek Schade on 08/12/2020.
//

import UIKit

#if DEBUG
import SwiftUI

@available(iOS 13, *)
extension UIViewController {
    private struct Preview: UIViewControllerRepresentable {
        let viewController: UIViewController

        func makeUIViewController(context: Context) -> UIViewController {
            return viewController
        }

        func updateUIViewController(_ uiViewController: UIViewController, context: Context) { }
    }

    var preview: some View {
        Preview(viewController: self)
    }
}
#endif
