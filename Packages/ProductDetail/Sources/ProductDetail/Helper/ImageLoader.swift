import Foundation
import UIKit

enum ImageLoader {
    static func load(
        url: String,
        imageView: UIImageView,
        queue: DispatchQueue = .global(),
        mainQueue: DispatchQueue = .main
    ) {
        queue.async {
            if let url = URL(string: url), let data = try? Data(contentsOf: url) {
                if let image = UIImage(data: data) {
                    mainQueue.async {
                        imageView.image = image
                    }
                }
            }
        }
    }
}
