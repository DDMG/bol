//
//  JSONLoader.swift
//  Bol
//
//  Created by Derek Schade on 08/12/2020.
//

import Foundation

extension Decodable {
    static func load(
        fileName: String,
        withExtension extension: String = "json",
        decoder: JSONDecoder = JSONDecoder()
    ) -> Self? {
        guard let url = Bundle.module.url(forResource: fileName, withExtension: `extension`) else { return nil }
        do {
            let data = try Data(contentsOf: url)
            return try decoder.decode(Self.self, from: data)
        } catch {
            print(error)
        }
        return nil
    }
}
