import Foundation
import UIKit

enum AlertPresenter {
    static func present(
        error: Error,
        viewController: UIViewController
    ) {
        let alert = UIAlertController(
            title: "Something went wrong 🤕",
            message: error.localizedDescription,
            preferredStyle: .alert
        )
        alert.addAction(UIAlertAction.init(title: "OK", style: .default, handler: { _ in }))
        viewController.present(alert, animated: true, completion: { })
    }
}
