//
//  ViewDelegate.swift
//  Bol
//
//  Created by Derek Schade on 08/12/2020.
//

import Foundation

/// Abstraction of UIViewController's lifecycle methods
public protocol ViewDelegate {
    func viewDidLoad()
}
