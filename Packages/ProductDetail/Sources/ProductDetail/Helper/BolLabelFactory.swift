//
//  BolLabelFactory.swift
//  Bol
//
//  Created by Derek Schade on 08/12/2020.
//

import Foundation
import UIKit

public enum BolLabelFactory {
    public enum Style {
        case title
        case subtitle
        case price
        case body
    }
    
    static public func label(
        _ style: Style
    ) -> UILabel {
        let label = UILabel()
        switch style {
            case .title:
                label.font = .systemFont(ofSize: 18, weight: .semibold)
            case .subtitle:
                label.font = .systemFont(ofSize: 16, weight: .semibold)
            case .price:
                label.font = .systemFont(ofSize: 24, weight: .semibold)
                label.textColor = .red
            case .body:
                label.font = .systemFont(ofSize: 16, weight: .regular)
        }
        return label
    }
}
