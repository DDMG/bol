//
//  ProductDetailEnvironment.swift
//  Bol
//
//  Created by Derek Schade on 08/12/2020.
//

import Foundation
import DTO

public struct ProductDetailEnvironment {
    var getProducts: (@escaping (Result<Products, Error>) -> ()) -> ()

    public init(
        getProducts: @escaping (@escaping ((Result<Products, Error>) -> ())) -> ()
    ) {
        self.getProducts = getProducts
    }
}
