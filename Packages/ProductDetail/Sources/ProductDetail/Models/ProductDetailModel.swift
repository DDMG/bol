//
//  ProductDetailModel.swift
//  Bol
//
//  Created by Derek Schade on 08/12/2020.
//

import Foundation

struct ProductDetailModel: Equatable {
    let title: String
    let price: String
    let producer: String
    let rating: Double
    let imageUrls: [String]
}
