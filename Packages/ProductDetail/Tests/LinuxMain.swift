import XCTest

import ProductDetailTests

var tests = [XCTestCaseEntry]()
tests += ProductDetailTests.allTests()
XCTMain(tests)
