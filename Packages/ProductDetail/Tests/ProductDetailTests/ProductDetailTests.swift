import XCTest
@testable import ProductDetail

final class ProductDetailTests: XCTestCase {
    
    class Delegate: ProductDetailViewModelDelegate {
        func setLoading(flag: Bool) { }
        
        var setResultInvoked: Bool = false
        var setResultInvokedParameter: Result<ProductDetailModel, Error>? = nil
        func set(result: Result<ProductDetailModel, Error>?) {
            setResultInvoked = true
            setResultInvokedParameter = result
        }
    }
    
    func test_viewDidLoad_shouldInvokedGetProducts() {
        //Given
        let exp = expectation(description: "getProducts invoked")
        
        let sut = ProductDetailViewModel.init(
            environment: .init(getProducts: { completion in
                exp.fulfill()
            })
        )
        
        // When
        sut.viewDidLoad()
        
        // Then
        XCTAssertTrue(sut.isLoading)
        waitForExpectations(timeout: 1.0, handler: nil)
    }
    
    func test_viewDidLoad_withSuccessCall_shouldInvokedParser() {
        //Given
        let delegate = Delegate()
        let mock = ProductDetailModel(title: "", price: "", producer: "", rating: 1, imageUrls: [])
        let exp = expectation(description: "parser invoked")
        let sut = ProductDetailViewModel.init(
            environment: .success,
            parser: { _ in
                exp.fulfill()
                return mock
            }
        )
        sut.delegate = delegate
        
        // When
        sut.viewDidLoad()
        
        // Then
        XCTAssertEqual(try? delegate.setResultInvokedParameter?.get(), mock)
        waitForExpectations(timeout: 1.0, handler: nil)
    }
    
    func test_viewDidLoad_withFailureCall_shouldReceiveError() {
        //Given
        struct MockError: Error, Equatable { }
        let exp = expectation(description: "error handled")
        let delegate = Delegate()
        
        let sut = ProductDetailViewModel.init(
            environment: .init(getProducts: { $0(.failure(MockError())) })
        )
        sut.delegate = delegate
        
        // When
        sut.viewDidLoad()
        
        // Then
        do {
            _ = try delegate.setResultInvokedParameter?.get()
        } catch {
            if error is MockError {
                exp.fulfill()
            }
        }
        waitForExpectations(timeout: 1.0, handler: nil)
    }
    
    // MARK: - View tests
    
    func test_viewDidLoad_withFailureCall_shouldShowAlert() {
        // Given
        let exp = expectation(description: "alertPresenter invoked")
        struct MockError: Error, Equatable { }
        let sut = ProductDetail.build(
            viewModel: ProductDetailViewModel(
                environment: .init(getProducts: { $0(.failure(MockError())) })
            ),
            viewBuilder: { viewModel in
                ProductDetailViewController(
                    viewModel: viewModel,
                    alertPresenter: { error, _ in
                        exp.fulfill()
                    }
                )
            }
        )
        
        // When
        // Trigger viewDidLoad
        _ = sut.view
        
        // Then
        waitForExpectations(timeout: 1.0, handler: nil)
    }
    
    func test_setResult_shouldSetTitleLabel() {
        struct MockVM: ProductDetailViewModelInterface {
            func viewDidLoad() { }
        }
        // Given
        let mockLabel = UILabel()
        let title = UUID().uuidString
        let mock = ProductDetailModel(title: title, price: "", producer: "", rating: 1, imageUrls: [""])
        
        let sut = ProductDetailViewController(viewModel: MockVM(), titleLabel: mockLabel)
        
        // When
        sut.set(result: .success(mock))
        
        // Then
        XCTAssertEqual(mockLabel.text, title)
    }
}
