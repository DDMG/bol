import API
import Foundation

public enum BolEndpoint: EndPoint {
    case products(id: Int)
    case recommendation(id: Int)
    case relatedProducts(id: Int)
    
    public var endPoint: URL {
        switch self {
            case .products(id: let id):
                return URL(string: "/catalog/v4/products/\(id)/")!
            case .recommendation(id: let id):
                return URL(string: "/catalog/v4/recommendations/\(id)/")!
            case .relatedProducts(id: let id):
                return URL(string: "/catalog/v4/relatedproducts/\(id)/")!
        }
    }
}

// TODO: refactor to read from plist
public let bolApi = API<BolEndpoint>(
    session: .shared,
    environment: .init(
        host: URL(string: "https://api.bol.com")!,
        apiKey: "D55EFE39C4EB4E04A50A65D2932C6127"
    ),
    urlFormatter: BolURLFormatter.format(environment:url:)
)
.debug(true, errorLogger: { print($0) })
