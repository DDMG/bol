//
//  File.swift
//  
//
//  Created by Derek Schade on 09/12/2020.
//

import Foundation
import API

enum BolURLFormatter {
    static func format(
        environment: Environment,
        url: URL
    ) -> URL? {
        guard var urlComponents = URLComponents(string: environment.host.absoluteString) else {
            return nil
        }
        urlComponents.path = url.absoluteString
        urlComponents.queryItems = [
            "apikey": environment.apiKey,
            "format": "json"
        ].map(URLQueryItem.init(name:value:))
        return urlComponents.url
    }
}
