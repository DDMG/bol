import XCTest
@testable import BolAPI
@testable import API

final class BolAPITests: XCTestCase {
    func test_bolURLFormatter() {
        // Given
        let host = URL(string: "https://api.bol.com")!
        let apiKey = UUID().uuidString
        let env = Environment(
            host: host,
            apiKey: apiKey
        )
        
        let path = URL(string: "/products/test")!
        let expectedResult = URL(string: "https://api.bol.com/products/test?apikey=\(apiKey)&format=json")
        
        // When
        let formattedUrl = BolURLFormatter.format(
            environment: env,
            url: path
        )
        
        // Then
        XCTAssertEqual(formattedUrl, expectedResult)
    }
}
