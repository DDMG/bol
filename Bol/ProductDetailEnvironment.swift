//
//  ProductDetailEnvironment.swift
//  Bol
//
//  Created by Derek Schade on 07/12/2020.
//

import BolAPI
import DTO
import API
import ProductDetail

extension ProductDetailEnvironment {
    
    /// Live/Production environment
    static func live(
        id: Int
    ) -> Self {
        .build(id: id, client: bolApi)
    }
    
    private static func build(
        id: Int,
        client: API<BolEndpoint>
    ) -> Self {
        .init { completion in
            client.fetch(.products(id: id)) { (result: Result<Products, Error>) in
                completion(result)
            }
        }
    }
}
