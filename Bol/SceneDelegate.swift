//
//  SceneDelegate.swift
//  Bol
//
//  Created by Derek Schade on 07/12/2020.
//

import UIKit
import SwiftUI
import ProductDetail

class SceneDelegate: UIResponder, UIWindowSceneDelegate {

    var window: UIWindow?


    func scene(_ scene: UIScene, willConnectTo session: UISceneSession, options connectionOptions: UIScene.ConnectionOptions) {
        if let windowScene = scene as? UIWindowScene {
            let window = UIWindow(windowScene: windowScene)
            window.backgroundColor = .white
            window.rootViewController = ProductDetail.build(
                environment: .live(id: 9200000028828094)
            )
            self.window = window
            window.makeKeyAndVisible()
        }
    }

}

