//
//  RatingView.swift
//  Bol
//
//  Created by Derek Schade on 07/12/2020.
//

import Foundation
import UIKit

final class RatingView: UIView {
    let stars: Int
    let rating: CGFloat
    
    private let stackView = UIStackView()
    
    public init(
        stars: Int,
        rating: CGFloat
    ) {
        assert(rating <= CGFloat(stars))
        assert(rating >= 0)
        self.stars = stars
        self.rating = rating
        super.init(frame: .zero)
        setupView()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}

#if DEBUG

import SwiftUI

extension RatingView: UIViewRepresentable {
    func makeUIView(context: Context) -> UIView {
        self
    }
    
    func updateUIView(_ view: UIView, context: Context) {
        
    }
}

struct RatingView_Previews: PreviewProvider {
    static var previews: some View {
        RatingView(stars: 5, rating: 4.7)
            .frame(width: 100, height: 100)
    }
}

#endif
