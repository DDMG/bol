//
//  ProductDetailViewController.swift
//  Bol
//
//  Created by Derek Schade on 07/12/2020.
//

import Foundation
import UIKit

final class ProductDetailViewController: UIViewController {
    
    private let viewModel: ProductDetailViewModel
    
    init(
        viewModel: ProductDetailViewModel
    ) {
        self.viewModel = viewModel
        super.init(nibName: nil, bundle: nil)
        self.viewModel.delegate = self
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}

extension ProductDetailViewController: ProductDetailViewModelDelegate {
    func setLoading(flag: Bool) {
        
    }
    
    func set(result: Result<ProductDetailViewModelModel, Error>?) {
        
    }
}
